from registry.gitlab.com/tclavier/docker/maven
#run mvn install
add . /srv/jersey-skeleton/
workdir /srv/jersey-skeleton/
run mvn install -Dmaven.test.skip=true
expose 8080
workdir /srv/jersey-skeleton/server
cmd mvn jetty:run
