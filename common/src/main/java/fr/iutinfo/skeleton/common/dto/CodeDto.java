package fr.iutinfo.skeleton.common.dto;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Classe permettant de gérer un objet "code DTO".
 */
public class CodeDto
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(CodeDto.class);
    
    
    
    // Attributs
    private int    id;
    private int    idCode;
    private int    idUser;
    private int    idLanguage;
    private String code;
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setIdCode(int idCode)
    {
        this.idCode = idCode;
    }
    
    public int getIdCode()
    {
        return this.idCode;
    }
    
    
    
    public void setIdUser(int idUser)
    {
        this.idUser = idUser;
    }
    
    public int getIdUser()
    {
        return this.idUser;
    }
    
    
    
    public void setIdLanguage(int idLanguage)
    {
        this.idLanguage = idLanguage;
    }
    
    public int getIdLanguage()
    {
        return this.idLanguage;
    }
    
    
    
    public void setCode(String code)
    {
        this.code = code;
    }
    
    public String getCode()
    {
        return this.code;
    }
    
    
    
}