package fr.iutinfo.skeleton.common.dto;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Classe permettant de gérer un objet "language DTO".
 */
public class LanguageDto
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(LanguageDto.class);
    
    
    
    // Attributs
    private int    id;
    private String name;
    private String picture;
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    
    
    public void setPicture(String picture)
    {
        this.picture = picture;
    }
    
    public String getPicture()
    {
        return this.picture;
    }
    
    
    
}