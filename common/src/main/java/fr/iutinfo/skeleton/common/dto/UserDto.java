package fr.iutinfo.skeleton.common.dto;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Classe permettant de gérer un objet "user DTO".
 */
public class UserDto
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(UserDto.class);
    
    
    
    // Attributs
    private int    id;
    private String name;
    private int    favoriteLanguage;
    private String email;
    private String password;
    private String alias;
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setFavoriteLanguage(int favoriteLanguage)
    {
        this.favoriteLanguage = favoriteLanguage;
    }
    
    public int getFavoriteLanguage()
    {
        return this.favoriteLanguage;
    }
    
    
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getEmail()
    {
        return this.email;
    }
    
    
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getPassword()
    {
        return this.password;
    }
    
    
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    
    
    public void setAlias(String alias)
    {
        this.alias = alias;
    }
    
    public String getAlias()
    {
        return this.alias;
    }
    
    
    
}