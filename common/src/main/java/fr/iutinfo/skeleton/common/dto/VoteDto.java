package fr.iutinfo.skeleton.common.dto;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Classe permettant de gérer un objet "vote DTO".
 */
public class VoteDto
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(VoteDto.class);
    
    
    
    // Attributs
    private int id;
    private int idCode;
    private int idUser;
    private int evaluation;
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setIdCode(int idCode)
    {
        this.idCode = idCode;
    }
    
    public int getIdCode()
    {
        return this.idCode;
    }
    
    
    
    public void setIdUser(int idUser)
    {
        this.idUser = idUser;
    }
    
    public int getIdUser()
    {
        return this.idUser;
    }
    
    
    
    public void setEvaluation(int evaluation)
    {
        this.evaluation = evaluation;
    }
    
    public int getEvaluation()
    {
        return this.evaluation;
    }
    
    
    
}