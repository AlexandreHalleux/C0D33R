package fr.iutinfo.skeleton.api;



import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.sqlite.SQLiteDataSource;

import javax.inject.Singleton;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;




@Singleton
public class BDDFactory
{
	
	
	
	// Journalisation
	//private final static Logger logger = LoggerFactory.getLogger(BDDFactory.class);
	
	
	
	// Constante
	private static final String DATABASE_NAME = "data.db";
	
	
	
	private static Jdbi jdbi = null;
	
	
	
	private static Jdbi getJdbi()
	{
		if(!exist())
		{
			SQLiteDataSource ds = new SQLiteDataSource();

			ds.setUrl("jdbc:sqlite:" + System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + DATABASE_NAME);

			jdbi = Jdbi.create(ds);

			jdbi.installPlugin(new SqlObjectPlugin());

			// Journalisations
			/*logger.debug("::: [DEBUG] user.dir : " + System.getProperty("user.dir"));
			logger.debug("::: [DEBUG] java.io.tmpdir : " + ystem.getProperty("java.io.tmpdir"));*/
		}

		return jdbi;
	}

	/**
	 * Méthode permettant de savoir si l'objet "jdbi" existe.
	 * 
	 * @return boolean
	 */
	public static boolean exist()
	{
		return jdbi != null;
	}

	static boolean tableExist(String tableName) throws SQLException
	{
		DatabaseMetaData dbm = getJdbi().open().getConnection().getMetaData();

		ResultSet tables = dbm.getTables(null, null, tableName, null);

		boolean exist = tables.next();

		tables.close();

		return exist;
	}

	public static <T>T buildDao(Class<T> daoClass)
	{
		return getJdbi().onDemand(daoClass);
	}
	
	
	
	public static void initFakeDB()
	{
		try
		{
			if(!tableExist("users"))
			{
				UserDao userDao = BDDFactory.buildDao(UserDao.class);
				// Création de la table
				userDao.createUserTable();
				userDao.insert(new User(1, "admin", "admin", 2));
				userDao.insert(new User(2, "pascal", "pascalou", 1));
				userDao.insert(new User(3, "quentin", "tarantino", 3));
				userDao.insert(new User(4, "alexandre", "leGrand", 3));
			}
		}
		catch(SQLException e)
		{
		}
		
		
		
		try
		{
			if(!tableExist("code"))
			{
				CodeDao codeDao = BDDFactory.buildDao(CodeDao.class);
				// Création de la table
				codeDao.createCodeTable();
				codeDao.insert(new Code(1, 1, 2, "#include <iostream>\n" + 
						"\n" + 
						"class complex {\n" + 
						"  double re, im;\n" + 
						"public:\n" + 
						"  complex(double r,double i)\n" + 
						"  {\n" + 
						"    re=r;\n" + 
						"    im=i;\n" + 
						"  }// construit un complexe à partir de deux scalaires\n" + 
						"  complex(double r)\n" + 
						"  {\n" + 
						"    re=r;\n" + 
						"    im=0;\n" + 
						"  }\n" + 
						"  complex()\n" + 
						"  {\n" + 
						"    re = im = 0;\n" + 
						"  }// complexe par défaut : (0,0)\n" + 
						"  double reel()\n" + 
						"  {\n" + 
						"    return re; \n" + 
						"  }\n" + 
						"  double imaginaire()\n" + 
						"  {\n" + 
						"    return im;\n" + 
						"  }\n" + 
						"  friend complex operator+(complex,complex);\n" + 
						"  friend complex operator-(complex,complex);//binaire\n" + 
						"  friend complex operator-(complex);//unaire\n" + 
						"  friend complex operator*(complex, complex);\n" + 
						"  friend complex operator/(complex, complex);\n" + 
						"\n" + 
						"  friend bool operator==(complex, complex);//egal\n" + 
						"  friend bool operator!=(complex, complex);//different\n" + 
						"};\n" + 
						"\n" + 
						"complex operator+(complex a1, complex a2)\n" + 
						"{\n" + 
						"  return complex(a1.re+a2.re,a1.im+a2.im);\n" + 
						"}\n" + 
						"\n" + 
						"complex operator-(complex a1,complex a2)\n" + 
						"{\n" + 
						"  return complex(a1.re-a2.re,a1.im-a2.im);\n" + 
						"}\n" + 
						"complex operator-(complex a)\n" + 
						"{\n" + 
						"  return complex(a.re*-1,a.im*-1);\n" + 
						"}\n" + 
						"\n" + 
						"complex operator*(complex a1, complex a2)\n" + 
						"{\n" + 
						"  return complex(a1.re*a2.re-a1.im*a2.im,a1.re*a2.im+a1.im*a2.re);\n" + 
						"}\n" + 
						"complex operator/(complex a1, complex a2)\n" + 
						"{\n" + 
						"  return complex();\n" + 
						"}\n" + 
						"\n" + 
						"bool operator==(complex a1, complex a2)\n" + 
						"{\n" + 
						"  return a1.re==a2.re&&a1.im==a2.im;\n" + 
						"}\n" + 
						"bool operator!=(complex a1, complex a2)\n" + 
						"{\n" + 
						"  return a1.re!=a2.re&&a1.im!=a2.im;\n" + 
						"}\n" + 
						"/*double complex::reel()\n" + 
						"{\n" + 
						"  return re;\n" + 
						"}\n" + 
						"\n" + 
						"double complex::imaginaire()\n" + 
						"{\n" + 
						"  return im;\n" + 
						"}\n" + 
						"*/\n" + 
						"int main()\n" + 
						"{\n" + 
						"  complex c1;\n" + 
						"  c1=complex();\n" + 
						"  std::cout<<\"Complexe c1=(0,0)\"<<c1.reel()<<\"+i\"<<c1.imaginaire()<<\"\\n\";\n" + 
						"  complex c2;\n" + 
						"  c2=complex(10,10)+complex(10,10);\n" + 
						"  std::cout<<\"Complexe c2=(10,10)+(10,10) \"<<c2.reel()<<\"+i\"<<c2.imaginaire()<<\"\\n\";\n" + 
						"  complex c3;\n" + 
						"  c3=complex(10,10)-complex(5,5);\n" + 
						"  std::cout<<\"Complexe c3=(10,10)-(5,5) \"<<c3.reel()<<\"+i\"<<c3.imaginaire()<<\"\\n\";\n" + 
						"  complex c4;\n" + 
						"  c4=complex(10,10)*complex(10,10);\n" + 
						"  std::cout<<\"Complexe c4=(10,10)*(10,10) \"<<c4.reel()<<\"+i\"<<c4.imaginaire()<<\"\\n\";\n" + 
						"\n" + 
						"}"));
				codeDao.insert(new Code(2, 1, 1, "import fr.iutinfo.skeleton.common.dto.LanguageDto;\n" + 
						"import org.slf4j.Logger;\n" + 
						"import org.slf4j.LoggerFactory;\n" + 
						"\n" + 
						"import javax.ws.rs.*;\n" + 
						"import javax.ws.rs.core.MediaType;\n" + 
						"import java.sql.SQLException;\n" + 
						"import java.util.List;\n" + 
						"import java.util.stream.Collectors;\n" + 
						"\n" + 
						"import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;\n" + 
						"\n" + 
						"\n" + 
						"\n" + 
						"@Path(\"/language\")\n" + 
						"@Produces(MediaType.APPLICATION_JSON)\n" + 
						"@Consumes(MediaType.APPLICATION_JSON)\n" + 
						"public class LanguageResource\n" + 
						"{\n" + 
						"	\n" + 
						"	\n" + 
						"	\n" + 
						"	// Journalisation\n" + 
						"    final static Logger logger = LoggerFactory.getLogger(LanguageResource.class);\n" + 
						"    \n" + 
						"    \n" + 
						"    \n" + 
						"    private static LanguageDao dao = BDDFactory.buildDao(LanguageDao.class);\n" + 
						"    \n" + 
						"    \n" + 
						"    \n" + 
						"    /**\n" + 
						"     * Constructeur.\n" + 
						"     */\n" + 
						"    public LanguageResource() throws SQLException\n" + 
						"    {\n" + 
						"        if(!tableExist(\"language\"))\n" + 
						"        {\n" + 
						"            // Journalisation\n" + 
						"            logger.debug(\"::: [DEBUG] CREATE TABLE [language] !\");\n" + 
						"            \n" + 
						"            // Création de la table\n" + 
						"            dao.createLanguageTable();\n" + 
						"        }\n" + 
						"    }\n" + 
						"    \n" + 
						"    \n" + 
						"    \n" + 
						"    @POST\n" + 
						"    public LanguageDto createLanguage(LanguageDto dto)\n" + 
						"    {\n" + 
						"    	Language language = new Language();\n" + 
						"    	\n" + 
						"    	language.initFromDto(dto);\n" + 
						"    	\n" + 
						"        int id = dao.insert(language);\n" + 
						"        \n" + 
						"        dto.setId(id);\n" + 
						"        \n" + 
						"        return dto;\n" + 
						"    }\n" + 
						"\n" + 
						"    @GET\n" + 
						"    @Path(\"/{id}\")\n" + 
						"    public LanguageDto getLanguage(@PathParam(\"id\") int id)\n" + 
						"    {\n" + 
						"    	Language language = dao.findById(id);\n" + 
						"    	\n" + 
						"        if(language == null)\n" + 
						"        {\n" + 
						"            throw new WebApplicationException(404);\n" + 
						"        }\n" + 
						"        \n" + 
						"        return language.convertToDto();\n" + 
						"    }\n" + 
						"\n" + 
						"    @GET\n" + 
						"    public List<LanguageDto> getAllLanguages(@QueryParam(\"q\") String query)\n" + 
						"    {\n" + 
						"        List<Language> list;\n" + 
						"        \n" + 
						"        if(query == null)\n" + 
						"        {\n" + 
						"            list = dao.all();\n" + 
						"        }\n" + 
						"        else\n" + 
						"        {\n" + 
						"            // Journalisation\n" + 
						"            logger.debug(\"::: [DEBUG] SEARCH LANGUAGES WITH QUERY : \" + query);\n" + 
						"            \n" + 
						"            // Requête\n" + 
						"            list = dao.search(\"%\" + query + \"%\");\n" + 
						"        }\n" + 
						"        \n" + 
						"        return list.stream().map(Language::convertToDto).collect(Collectors.toList());\n" + 
						"    }\n" + 
						"\n" + 
						"    @DELETE\n" + 
						"    @Path(\"/{id}\")\n" + 
						"    public void deleteLanguage(@PathParam(\"id\") int id)\n" + 
						"    {\n" + 
						"        // Requête\n" + 
						"        dao.delete(id);\n" + 
						"    }\n" + 
						"    \n" + 
						"    \n" + 
						"    \n" + 
						"}"));
				codeDao.insert(new Code(3, 1, 3, "document.querySelector(\"#reset\").addEventListener(\"click\", test);//innerHTML = \"&nbsp;\";\n" + 
						"\n" + 
						"function test()\n" + 
						"{\n" + 
						"	document.querySelector(\".error-container\").innerHTML = \"&nbsp;\";\n" + 
						"	\n" + 
						"	document.querySelector(\"#userlogin\").style.backgroundColor = \"rgba(255, 0, 0, 0)\";\n" + 
						"}\n" + 
						"\n" + 
						"\n" + 
						"\n" + 
						"document.querySelector(\".error-container\").innerHTML = \"&nbsp;\";\n" + 
						"\n" + 
						"function checkLogin()\n" + 
						"{\n" + 
						"	const error = document.querySelector(\".error-container\");\n" + 
						"	const val   = document.querySelector(\"#userlogin\");\n" + 
						"	\n" + 
						"	const regex = /^[A-Za-z0-9]{1,20}$/;\n" + 
						"	\n" + 
						"	if(val.value.length == 0)\n" + 
						"	{\n" + 
						"		error.innerHTML = \"Veuillez saisir un identifiant !\";\n" + 
						"		\n" + 
						"		val.style.backgroundColor = \"rgba(255, 0, 0, 0.1)\";\n" + 
						"		\n" + 
						"		return false;\n" + 
						"	}\n" + 
						"	else if(val.value.length < 6)\n" + 
						"	{\n" + 
						"		if(regex.test(val.value))\n" + 
						"		{\n" + 
						"			error.innerHTML = \"Doit être compris entre 6 et 20 caractères !\";\n" + 
						"			\n" + 
						"			val.style.backgroundColor = \"rgba(255, 0, 0, 0.1)\";\n" + 
						"		}\n" + 
						"		else\n" + 
						"		{\n" + 
						"			error.innerHTML = \"Doit être uniquement composé de lettre et/ou de chiffre !\";\n" + 
						"			\n" + 
						"			val.style.backgroundColor = \"rgba(255, 0, 0, 0.1)\";\n" + 
						"		}\n" + 
						"\n" + 
						"		return false;\n" + 
						"	}\n" + 
						"	else if(val.value.length >= 6 && val.value.length <= 20)\n" + 
						"	{\n" + 
						"		if(regex.test(val.value))\n" + 
						"		{\n" + 
						"			return true;\n" + 
						"		}\n" + 
						"		else\n" + 
						"		{\n" + 
						"			error.innerHTML= \"Doit être uniquement composé de lettre et/ou de chiffre !\";\n" + 
						"			\n" + 
						"			val.style.backgroundColor = \"rgba(255, 0, 0, 0.1)\";\n" + 
						"\n" + 
						"			return false;\n" + 
						"		}\n" + 
						"	}\n" + 
						"	else\n" + 
						"	{\n" + 
						"		error.innerHTML= \"Doit être compris entre 6 et 20 caractères !\";\n" + 
						"		\n" + 
						"		val.style.backgroundColor = \"rgba(255, 0, 0, 0.1)\";\n" + 
						"\n" + 
						"		return false;\n" + 
						"	}\n" + 
						"}"));
				codeDao.insert(new Code(4, 1, 4, "<body>Bonjour \"Jacque\" !</body>"));
				codeDao.insert(new Code(5, 1, 5, "101000110110110101010101010101010101010110101010011001010101100100110010101011010100010000000110101010101010101010101010101010100101011011111100000000000000000000000000000111111111111111111111111111"));
			}
		}
		catch(SQLException e)
		{
		}
		
		
		
		try
		{
			if(!tableExist("language"))
			{
				LanguageDao languageDao = BDDFactory.buildDao(LanguageDao.class);
				// Création de la table
				languageDao.createLanguageTable();
				languageDao.insert(new Language(1, "Java", "java.svg"));
				languageDao.insert(new Language(2, "C++", "c++.svg"));
				languageDao.insert(new Language(3, "JavaScript", "js.svg"));
				languageDao.insert(new Language(4, "HTML", "html.svg"));
				languageDao.insert(new Language(5, "Binaire", "binary.svg"));
			}
		}
		catch(SQLException e)
		{
		}
		
		
		
		try
		{
			if(!tableExist("vote"))
			{
				VoteDao voteDao = BDDFactory.buildDao(VoteDao.class);
				// Création de la table
				voteDao.createVoteTable();
				voteDao.insert(new Vote(1, 3, 2, 1));
				voteDao.insert(new Vote(2, 1, 2, 0));
				voteDao.insert(new Vote(3, 4, 1, 1));
				voteDao.insert(new Vote(4, 2, 3, 1));
			}
		}
		catch(SQLException e)
		{
		}
	}
	
	
	
}