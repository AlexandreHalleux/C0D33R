package fr.iutinfo.skeleton.api;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.iutinfo.skeleton.common.dto.CodeDto;



public class Code
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(Code.class);
    
    
    
    // Attributs
    private int    id;
    private int    idUser;
    private int    idLanguage;
    private String code;
    
    
    
    /**
     * Constructeur par défaut.
     */
    public Code()
    {
    }
    
    /**
     * Constructeur.
     */
    public Code(int id, int idUser, int idLanguage, String code)
    {
        this.id         = id;
        this.idUser     = idUser;
        this.idLanguage = idLanguage;
        this.code       = code;
    }
    
    
    
    @Override
    public String toString()
    {
        return "> ID............: " + this.id + "\n" + 
               "> ID USER.......: " + this.idUser +
               "> ID LANGUAGE...: " + this.idLanguage +
               "> CODE..........:\n" + this.code;
    }
    
    
    
    @Override
    public boolean equals(Object obj)
    {
        if(getClass() != obj.getClass())
        {
            return false;
        }
        
        Code code = (Code)obj;
        
        return this.id == code.getId() && this.idUser == code.getIdUser() && this.idLanguage == code.getIdLanguage() && this.code.equals(code.getCode());
    }
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setIdUser(int idUser)
    {
        this.idUser = idUser;
    }
    
    public int getIdUser()
    {
        return this.idUser;
    }
    
    
    
    public void setIdLanguage(int idLanguage)
    {
        this.idLanguage = idLanguage;
    }
    
    public int getIdLanguage()
    {
        return this.idLanguage;
    }
    
    
    
    public void setCode(String code)
    {
        this.code = code;
    }
    
    public String getCode()
    {
        return this.code;
    }



    public void initFromDto(CodeDto dto)
    {
        this.setId(dto.getId());
        this.setIdUser(dto.getIdUser());
        this.setIdLanguage(dto.getIdLanguage());
        this.setCode(dto.getCode());
    }
    
    public CodeDto convertToDto()
    {
    	CodeDto dto = new CodeDto();
        
        // id
        dto.setId(this.getId());
        // idUser
        dto.setIdUser(this.getIdUser());
        // idLanguage
        dto.setIdLanguage(this.getIdLanguage());
        // code
        dto.setCode(this.getCode());
        
        return dto;
    }
    
    
    
}