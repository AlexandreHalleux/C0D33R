package fr.iutinfo.skeleton.api;



import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;



public interface CodeDao
{
	
	
	
    @SqlUpdate("CREATE TABLE code (id integer PRIMARY KEY AUTOINCREMENT, idUser integer, idLanguage integer, code longtext)")
    void createCodeTable();
    
    
    
    @SqlUpdate("INSERT INTO code (idUser, idLanguage, code) VALUES (:idUser, :idLanguage, :code)")
    @GetGeneratedKeys
    int insert(@BindBean() Code code);
    
    
    
    @SqlUpdate("DROP TABLE IF EXISTS code")
    void dropCodeTable();
    
    
    
    @SqlUpdate("DELETE FROM code WHERE id = :id")
    void delete(@Bind("id") int id);
    
    
    
    @SqlQuery("SELECT * FROM code ORDER BY id")
    @RegisterBeanMapper(Code.class)
    List<Code> all();
    
    @SqlQuery("SELECT * FROM code WHERE SEARCH LIKE :idLanguage")
    @RegisterBeanMapper(Code.class)
    List<Code> search(@Bind("idLanguage") int idLanguage);
    
    
    
    @SqlQuery("SELECT * FROM code WHERE id = :id")
    @RegisterBeanMapper(Code.class)
    Code findById(@Bind("id") int id);
    
    @SqlQuery("SELECT * FROM code WHERE idLanguage = :idLanguage")
    @RegisterBeanMapper(Code.class)
    Code findByName(@Bind("idLanguage") int idLanguage);
    
    
    
}