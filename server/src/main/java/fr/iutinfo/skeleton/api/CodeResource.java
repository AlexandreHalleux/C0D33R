package fr.iutinfo.skeleton.api;



import fr.iutinfo.skeleton.common.dto.CodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;



@Path("/code")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CodeResource
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(CodeResource.class);
    
    
    
    private static CodeDao dao = BDDFactory.buildDao(CodeDao.class);
    
    
    
    /**
     * Constructeur.
     */
    public CodeResource() throws SQLException
    {
        if(!tableExist("code"))
        {
            // Journalisation
            logger.debug("::: [DEBUG] CREATE TABLE [code] !");
            
            // Création de la table
            dao.createCodeTable();
        }
    }
    
    
    
    @POST
    public CodeDto createCode(CodeDto dto)
    {
        Code code = new Code();
        
        code.initFromDto(dto);
        
        int id = dao.insert(code);
        
        dto.setId(id);
        
        return dto;
    }

    @GET
    @Path("/{name}")
    public CodeDto getCode(@PathParam("id") int id)
    {
        Code code = dao.findById(id);
        
        if(code == null)
        {
            throw new WebApplicationException(404);
        }
        
        return code.convertToDto();
    }

    @GET
    public List<CodeDto> getAllCodes(@QueryParam("q") String query)
    {
        List<Code> list;
        
        if(query == null)
        {
            list = dao.all();
        }
        else
        {
            // Journalisation
            logger.debug("::: [DEBUG] SEARCH WITH QUERY : " + query);
            
            // Requête
            //list = dao.search("%" + query + "%");
            list = null;
        }
        
        return list.stream().map(Code::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteCode(@PathParam("id") int id)
    {
        // Requête
        dao.delete(id);
    }
    
    
    
}