package fr.iutinfo.skeleton.api;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.iutinfo.skeleton.common.dto.LanguageDto;



public class Language
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(Language.class);
    
    
    
    // Attributs
    private int    id;
    private String name;
    private String picture;
    
    
    
    /**
     * Constructeur par défaut.
     */
    public Language()
    {
    }
    
    /**
     * Constructeur.
     */
    public Language(int id, String name, String picture)
    {
        this.id      = id;
        this.name    = name;
        this.picture = picture;
    }
    
    
    
    @Override
    public String toString()
    {
        return "> ID........: " + this.id + "\n" +
               "> NAME......: " + this.name + "\n" +
               "> PICTURE...: " + this.picture;
    }
    
    
    
    @Override
    public boolean equals(Object obj)
    {
        if(getClass() != obj.getClass())
        {
            return false;
        }
        
        Language language = (Language)obj;
        
        return this.id == language.getId() && this.name.equals(language.getName()) && this.picture.equals(language.getPicture());
    }
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    
    
    public void setPicture(String picture)
    {
        this.picture = picture;
    }
    
    public String getPicture()
    {
        return this.picture;
    }
    
    
    
    public void initFromDto(LanguageDto dto)
    {
        this.setId(dto.getId());
        this.setName(dto.getName());
        this.setPicture(dto.getPicture());
    }
    
    public LanguageDto convertToDto()
    {
        LanguageDto dto = new LanguageDto();
        
        // id
        dto.setId(this.getId());
        // name
        dto.setName(this.getName());
        // picture
        dto.setPicture(this.getPicture());
        
        return dto;
    }
    
    
    
}
