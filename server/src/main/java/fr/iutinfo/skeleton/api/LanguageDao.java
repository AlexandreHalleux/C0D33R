package fr.iutinfo.skeleton.api;



import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;



public interface LanguageDao
{
	
	
	
    @SqlUpdate("CREATE TABLE language (id integer PRIMARY KEY AUTOINCREMENT, name varchar(100), picture varchar(100))")
    void createLanguageTable();
    
    
    
    @SqlUpdate("INSERT INTO language (name, picture) VALUES (:name, :picture)")
    @GetGeneratedKeys
    int insert(@BindBean() Language language);
    
    
    
    @SqlUpdate("DROP TABLE IF EXISTS language")
    void dropLanguageTable();
    
    
    
    @SqlUpdate("DELETE FROM language WHERE id = :id")
    void delete(@Bind("id") int id);
    
    
    
    @SqlQuery("SELECT * FROM language ORDER BY id")
    @RegisterBeanMapper(Language.class)
    List<Language> all();
    
    @SqlQuery("SELECT * FROM language WHERE SEARCH LIKE :name")
    @RegisterBeanMapper(Language.class)
    List<Language> search(@Bind("name") String name);
    
    
    
    @SqlQuery("SELECT * FROM language WHERE id = :id")
    @RegisterBeanMapper(Language.class)
    Language findById(@Bind("id") int id);
    
    @SqlQuery("SELECT * FROM language WHERE name = :name")
    @RegisterBeanMapper(Language.class)
    Language findByName(@Bind("name") String name);
    
    
    
}