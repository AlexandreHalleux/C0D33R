package fr.iutinfo.skeleton.api;



import fr.iutinfo.skeleton.common.dto.LanguageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;



@Path("/language")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LanguageResource
{
	
	
	
	// Journalisation
    final static Logger logger = LoggerFactory.getLogger(LanguageResource.class);
    
    
    
    private static LanguageDao dao = BDDFactory.buildDao(LanguageDao.class);
    
    
    
    /**
     * Constructeur.
     */
    public LanguageResource() throws SQLException
    {
        if(!tableExist("language"))
        {
            // Journalisation
            logger.debug("::: [DEBUG] CREATE TABLE [language] !");
            
            // Création de la table
            dao.createLanguageTable();
        }
    }
    
    
    
    @POST
    public LanguageDto createLanguage(LanguageDto dto)
    {
    	Language language = new Language();
    	
    	language.initFromDto(dto);
    	
        int id = dao.insert(language);
        
        dto.setId(id);
        
        return dto;
    }

    @GET
    @Path("/{id}")
    public LanguageDto getLanguage(@PathParam("id") int id)
    {
    	Language language = dao.findById(id);
    	
        if(language == null)
        {
            throw new WebApplicationException(404);
        }
        
        return language.convertToDto();
    }

    @GET
    public List<LanguageDto> getAllLanguages(@QueryParam("q") String query)
    {
        List<Language> list;
        
        if(query == null)
        {
            list = dao.all();
        }
        else
        {
            // Journalisation
            logger.debug("::: [DEBUG] SEARCH LANGUAGES WITH QUERY : " + query);
            
            // Requête
            list = dao.search("%" + query + "%");
        }
        
        return list.stream().map(Language::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteLanguage(@PathParam("id") int id)
    {
        // Requête
        dao.delete(id);
    }
    
    
    
}