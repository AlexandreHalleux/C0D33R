package fr.iutinfo.skeleton.api;



import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import fr.iutinfo.skeleton.common.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import java.security.SecureRandom;



public class User implements Principal
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(User.class);
    
    
    
    private static User anonymous = new User(-1, "Anonymous", "anonym", -1);
    
    // Attributs
    private int    id;
    private String name;
    private int    favoriteLanguage;
    private String alias;
    private String email;
    private String password;
    private String passwdHash;
    private String salt;
    private String search;
    
    
    
    /**
     * Constructeur par défaut.
     */
    public User()
    {
    }
    
    /**
     * Constructeur.
     * @param id
     * @param name
     */
    public User(int id, String name)
    {
        this.id   = id;
        this.name = name;
    }
    
    /**
     * Constructeur.
     * @param id
     * @param name
     * @param alias
     */
    public User(int id, String name, String alias, int favoriteLanguage)
    {
        this(id, name);
        
        this.alias            = alias;
        this.favoriteLanguage = favoriteLanguage;
    }
    
    
    
    @Override
    public String toString()
    {
        return "> ID..................: " + this.id + "\n" +
               "> ALIAS...............: " + this.alias + "\n" +
               "> NAME................: " + this.name + "\n" +
               "> EMAIL...............: " + this.email + "\n" +
               "> FAVORITE LANGUAGE...: " + this.favoriteLanguage;
    }
    
    
    
    @Override
    public boolean equals(Object obj)
    {
        if(getClass() != obj.getClass())
        {
            return false;
        }
        
        User user = (User)obj;
        
        return this.id == user.getId() && this.favoriteLanguage == user.getFavoriteLanguage() && this.name.equals(user.getName()) && this.alias.equals(user.getAlias()) && this.email.equals(user.getEmail()) && this.passwdHash.equals(user.getPasswdHash()) && this.salt.equals((user.getSalt()));
    }
    
    
    
    public static User getAnonymousUser()
    {
        return anonymous;
    }
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setFavoriteLanguage(int favoriteLanguage)
    {
        this.favoriteLanguage = favoriteLanguage;
    }
    
    public int getFavoriteLanguage()
    {
        return this.favoriteLanguage;
    }
    
    
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getEmail()
    {
        return this.email;
    }
    
    
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    
    
    public void setPassword(String password)
    {
        this.passwdHash = buildHash(password, getSalt());
        
        this.password = password;
    }
    
    public String getPassword()
    {
        return this.password;
    }
    
    
    
    private String buildHash(String password, String s)
    {
        Hasher hasher = Hashing.sha256().newHasher();
        
        hasher.putString(password + s, Charsets.UTF_8);
        
        return hasher.hash().toString();
    }

    public boolean isGoodPassword(String password)
    {
        if(isAnonymous())
        {
            return false;
        }
        
        String hash = buildHash(password, getSalt());
        
        return hash.equals(getPasswdHash());
    }

    public String getPasswdHash()
    {
        return this.passwdHash;
    }

    public void setPasswdHash(String passwdHash)
    {
        this.passwdHash = passwdHash;
    }
    
    
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        
        int result = 1;
        
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((passwdHash == null) ? 0 : passwdHash.hashCode());
        result = prime * result + ((salt == null) ? 0 : salt.hashCode());
        // FAVORITE LANGUAGE ???
        
        return result;
    }
    
    
    
    public void setAlias(String alias)
    {
        this.alias = alias;
    }
    
    public String getAlias()
    {
        return this.alias;
    }
    
    
    
    public void setSalt(String salt)
    {
        this.salt = salt;
    }
    
    public String getSalt()
    {
        if(this.salt == null)
        {
        	this.salt = generateSalt();
        }
        
        return this.salt;
    }
    
    
    
    private String generateSalt()
    {
        SecureRandom random = new SecureRandom();
        
        Hasher hasher = Hashing.sha256().newHasher();
        
        hasher.putLong(random.nextLong());
        
        return hasher.hash().toString();
    }

    public void resetPasswordHash()
    {
        if(password != null && !password.isEmpty())
        {
            setPassword(getPassword());
        }
    }

    public boolean isInUserGroup()
    {
        return !(this.id == anonymous.getId());
    }

    public boolean isAnonymous()
    {
        return this.getId() == getAnonymousUser().getId();
    }

    public String getSearch()
    {
        search = name + " " + alias + " " + email;
        
        return search;
    }

    public void setSearch(String search)
    {
        this.search = search;
    }
    
    
    
    public void initFromDto(UserDto dto)
    {
        this.setAlias(dto.getAlias());
        this.setEmail(dto.getEmail());
        this.setId(dto.getId());
        this.setFavoriteLanguage(dto.getFavoriteLanguage());
        this.setName(dto.getName());
        this.setPassword(dto.getPassword());
    }

    public UserDto convertToDto()
    {
        UserDto dto = new UserDto();
        
        // id
        dto.setId(this.getId());
        // alias
        dto.setAlias(this.getAlias());
        // email
        dto.setEmail(this.getEmail());
        // name
        dto.setName(this.getName());
        // password
        dto.setPassword(this.getPassword());
        // favoriteLanguage
        dto.setFavoriteLanguage(this.getFavoriteLanguage());
        
        return dto;
    }
    
    
    
}