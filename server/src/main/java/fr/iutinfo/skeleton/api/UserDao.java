package fr.iutinfo.skeleton.api;



import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;



public interface UserDao
{
    
    
    
    @SqlUpdate("CREATE TABLE users (id integer PRIMARY KEY AUTOINCREMENT, name varchar(100), alias varchar(100), email varchar(100), favoriteLanguage integer, passwdHash varchar(64), salt varchar(64), search varchar(1024))")
    void createUserTable();
    
    
    
    @SqlUpdate("INSERT INTO users (name, alias, email, favoriteLanguage, passwdHash, salt, search) VALUES (:name, :alias, :email, :favoriteLanguage, :passwdHash, :salt, :search)")
    @GetGeneratedKeys
    int insert(@BindBean() User user);
    
    
    
    @SqlUpdate("DROP TABLE IF EXISTS users")
    void dropUserTable();
    
    
    
    @SqlUpdate("DELETE FROM users WHERE id = :id")
    void delete(@Bind("id") int id);
    
    
    
    @SqlQuery("SELECT * FROM users ORDER BY id")
    @RegisterBeanMapper(User.class)
    List<User> all();
    
    @SqlQuery("SELECT * FROM users WHERE SEARCH LIKE :name")
    @RegisterBeanMapper(User.class)
    List<User> search(@Bind("name") String name);
    
    
    
    @SqlQuery("SELECT * FROM users WHERE id = :id")
    @RegisterBeanMapper(User.class)
    User findById(@Bind("id") int id);
    
    @SqlQuery("SELECT * FROM users WHERE name = :name")
    @RegisterBeanMapper(User.class)
    User findByName(@Bind("name") String name);
    
    
    
}