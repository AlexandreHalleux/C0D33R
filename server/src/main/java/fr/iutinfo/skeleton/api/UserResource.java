package fr.iutinfo.skeleton.api;



import fr.iutinfo.skeleton.common.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;



@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(UserResource.class);
    
    
    
    private static UserDao dao = BDDFactory.buildDao(UserDao.class);
    
    
    
    /**
     * Constructeur.
     */
    public UserResource() throws SQLException
    {
        if(!tableExist("users"))
        {
            // Journalisation
            logger.debug("::: [DEBUG] CREATE TABLE [users] !");
            
            // Création de la table
            dao.createUserTable();
        }
    }
    
    
    
    @POST
    public UserDto createUser(UserDto dto)
    {
        User user = new User();
        
        user.initFromDto(dto);
        
        user.resetPasswordHash();
        
        int id = dao.insert(user);
        
        dto.setId(id);
        
        return dto;
    }

    @GET
    @Path("/{name}")
    public UserDto getUser(@PathParam("name") String name)
    {
        User user = dao.findByName(name);
        
        if(user == null)
        {
            throw new WebApplicationException(404);
        }
        
        return user.convertToDto();
    }

    @GET
    public List<UserDto> getAllUsers(@QueryParam("q") String query)
    {
        List<User> list;
        
        if(query == null)
        {
            list = dao.all();
        }
        else
        {
            // Journalisation
            logger.debug("::: [DEBUG] SEARCH WITH QUERY : " + query);
            
            // Requête
            list = dao.search("%" + query + "%");
        }
        
        return list.stream().map(User::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") int id)
    {
        // Requête
        dao.delete(id);
    }
    
    
    
}