package fr.iutinfo.skeleton.api;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.iutinfo.skeleton.common.dto.VoteDto;



public class Vote
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(Vote.class);
    
    
    
    // Attributs
    private int id;
    private int idCode;
    private int idUser;
    private int evaluation; // 1 = AIME // 0 = N'AIME PAS
    
    
    
    /**
     * Constructeur par défaut.
     */
    public Vote()
    {
    }
    
    /**
     * Constructeur.
     */
    public Vote(int id, int idCode, int idUser, int evaluation)
    {
    	this.id         = id;
        this.idCode     = idCode;
        this.idUser     = idUser;
        this.evaluation = evaluation;
    }
    
    
    
    @Override
    public String toString()
    {
        return "> ID...........: " + this.id + "\n" +
               "> ID CODE......: " + this.idCode + "\n" +
               "> ID USER......: " + this.idUser + "\n" +
               "> EVALUATION...: " + this.evaluation;
    }
    
    
    
    @Override
    public boolean equals(Object obj)
    {
        if(getClass() != obj.getClass())
        {
            return false;
        }
        
        Vote vote = (Vote)obj;
        
        return this.id == vote.getId() && this.idCode == vote.getIdCode() && this.idUser == vote.getIdUser() && this.evaluation == vote.getEvaluation();
    }
    
    
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    
    
    public void setIdCode(int idCode)
    {
        this.idCode = idCode;
    }
    
    public int getIdCode()
    {
        return this.idCode;
    }
    
    
    
    public void setIdUser(int idUser)
    {
        this.idUser = idUser;
    }
    
    public int getIdUser()
    {
        return this.idUser;
    }
    
    
    
    public void setEvaluation(int evaluation)
    {
        this.evaluation = evaluation;
    }
    
    public int getEvaluation()
    {
        return this.evaluation;
    }
    
    
    
    /**
     * Méthode permettant de convertir le DTO en objet.
     */
    public void initFromDto(VoteDto dto)
    {
        this.setId(dto.getId());
        this.setIdCode(dto.getIdCode());
        this.setIdUser(dto.getIdUser());
        this.setEvaluation(dto.getEvaluation());
    }
    
    /**
     * Méthode permettant de convertir l'objet en DTO.
     */
    public VoteDto convertToDto()
    {
    	VoteDto dto = new VoteDto();
        
        // id
        dto.setId(this.getId());
        // idCode
        dto.setIdCode(this.getIdCode());
        // idUser
        dto.setIdUser(this.getIdUser());
        // evaluation
        dto.setEvaluation(this.getEvaluation());
        
        return dto;
    }
    
    
    
}