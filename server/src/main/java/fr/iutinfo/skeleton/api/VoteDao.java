package fr.iutinfo.skeleton.api;



import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;



public interface VoteDao
{
	
	
	
    @SqlUpdate("CREATE TABLE vote (id integer PRIMARY KEY AUTOINCREMENT, idCode integer, idUser integer, evaluation integer)")
    void createVoteTable();
    
    
    
    @SqlUpdate("INSERT INTO vote (idCode, idUser, evaluation) VALUES (:idCode, :idUser, :evaluation)")
    @GetGeneratedKeys
    int insert(@BindBean() Vote vote);
    
    
    
    @SqlUpdate("DROP TABLE IF EXISTS vote")
    void dropVoteTable();
    
    
    
    @SqlUpdate("DELETE FROM vote WHERE id = :id")
    void delete(@Bind("id") int id);
    
    
    
    @SqlQuery("SELECT * FROM vote ORDER BY id")
    @RegisterBeanMapper(Vote.class)
    List<Vote> all();
    
    @SqlQuery("SELECT * FROM vote WHERE SEARCH LIKE :name")
    @RegisterBeanMapper(Vote.class)
    List<Vote> search(@Bind("name") String name);
    
    
    
    @SqlQuery("SELECT * FROM vote WHERE id = :id")
    @RegisterBeanMapper(Vote.class)
    Vote findById(@Bind("id") int id);
    
    
    
}