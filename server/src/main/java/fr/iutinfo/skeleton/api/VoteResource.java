package fr.iutinfo.skeleton.api;



import fr.iutinfo.skeleton.common.dto.VoteDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;



@Path("/vote")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class VoteResource
{
    
    
    
    // Journalisation
    final static Logger logger = LoggerFactory.getLogger(VoteResource.class);
    
    
    
    private static VoteDao dao = BDDFactory.buildDao(VoteDao.class);
    
    
    
    /**
     * Constructeur.
     */
    public VoteResource() throws SQLException
    {
        if (!tableExist("vote"))
        {
            // Journalisation
            logger.debug("::: [DEBUG] CREATE TABLE [vote] !");
            
            // Création de la table
            dao.createVoteTable();
        }
    }
    
    
    
    @POST
    public VoteDto createVote(VoteDto dto)
    {
    	Vote vote = new Vote();
    	
        vote.initFromDto(dto);
        
        int id = dao.insert(vote);
        
        dto.setId(id);
        
        return dto;
    }
    
    @POST
    @Path("/{idUser}/{idCode}/like")
    public VoteDto createVoteLike(@PathParam("idUser") int idUser, @PathParam("idCode") int idCode,VoteDto dto)
    {
    	Vote vote = new Vote();
    	System.out.println(dto.getIdCode()+"   "+dto.getIdUser());
        vote.initFromDto(dto);
        vote.setEvaluation(1);
        int id = dao.insert(vote);
        
        dto.setId(id);
        
        return dto;
    }
    
    
    
    
    
    @POST
    @Path("/{idUser}/{idCode}/dislike")
    public VoteDto createVoteDisLike(@PathParam("idUser") int idUser, @PathParam("idCode") int idCode,VoteDto dto)
    {
    	Vote vote = new Vote();
    	System.out.println(dto.getIdCode()+"   "+dto.getIdUser());
        vote.initFromDto(dto);
        vote.setEvaluation(0);
        int id = dao.insert(vote);
        
        dto.setId(id);
        
        return dto;
    }
    
    

    @GET
    @Path("/{id}")
    public VoteDto getVote(@PathParam("id") int id)
    {
    	Vote vote = dao.findById(id);
    	
        if(vote == null)
        {
            throw new WebApplicationException(404);
        }
        
        return vote.convertToDto();
    }

    @GET
    public List<VoteDto> getAllVotes(@QueryParam("q") String query)
    {
        List<Vote> list;
        
        if(query == null)
        {
            list = dao.all();
        }
        else
        {
            // Journalisation
            logger.debug("::: [DEBUG] SEARCH VOTES WITH QUERY : " + query);
            
            // Requête
            list = dao.search("%" + query + "%");
        }
        
        return list.stream().map(Vote::convertToDto).collect(Collectors.toList());
    }

    @DELETE
    @Path("/{id}")
    public void deleteVote(@PathParam("id") int id)
    {
        // Requête
        dao.delete(id);
    }
    
    
    
}