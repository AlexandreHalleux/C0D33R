package fr.iutinfo.skeleton.auth;



import fr.iutinfo.skeleton.api.User;
import fr.iutinfo.skeleton.api.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import static fr.iutinfo.skeleton.api.BDDFactory.buildDao;



@Provider
@PreMatching
public class AuthFilter implements ContainerRequestFilter
{
    
    
    
    // Journalisation
    private final static Logger logger = LoggerFactory.getLogger(AuthFilter.class);
    
    
    
    @Override
    public void filter(ContainerRequestContext containerRequest) throws WebApplicationException
    {
    	
    	
    	
        String authorizationHeader = containerRequest.getHeaderString(HttpHeaders.AUTHORIZATION);
        String scheme = containerRequest.getUriInfo().getRequestUri().getScheme();
        
        // Journalisation
        logger.debug("authorizationHeader : " + authorizationHeader);

        if(authorizationHeader != null)
        {
            String[] loginPassword = BasicAuth.decode(authorizationHeader);
            checkLoginPassword(loginPassword);
            String login = loginPassword[0];
            String password = loginPassword[1];
            logger.debug("login : " + login + ", password : " + password);
            User user = loadUserFromLogin(login);
            
            // ici on teste le password avant
            
            // Journalisation
            logger.debug("good password !");
            
            containerRequest.setSecurityContext(new AppSecurityContext(user, scheme));
    }}

    private User loadUserFromLogin(String login)
    {
        UserDao dao = buildDao(UserDao.class);
        User user = dao.findByName(login);
        
        if(user == null)
        {
        	int newId = dao.all().size()+1;
        	user = new User(newId,login);
        	dao.insert(user);
        }
        
        return user;
    }

    private void checkLoginPassword(String[] loginPassword)
    {
        if(loginPassword == null || loginPassword.length != 2)
        {
            throw new WebApplicationException(Status.NOT_ACCEPTABLE);
        }
    }
    
    
    
}