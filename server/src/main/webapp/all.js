function getUser(name)
{
	getUserGeneric(name, "v1/user/");
}

function getUserGeneric(name, url)
{
	$.getJSON(url + name, function(data)
	{
		afficheUser(data);
	});
}

function chargeProfile(name)
{
	$.getJSON("v1/user/"+ name, function(data)
	{
	});			
}


function chargeCode()
{
	$.getJSON("v1/code", function(data)
	{
		var listeCode="";
		for(let i=0;i<data.length;i++)
		{
			if(i==0){
				listeCode+="<li value='"+data[i]["id"]+"' class='slide active' id='code"+data[i]["id"]+"'>" +
						"<div class='textareacontain'><textarea style='resize:none;' rows='10' cols='50' " +
						"readonly>"+data[i]["code"]+"</textarea></div></li>";
			}else{
				listeCode+="<li value='"+data[i]["id"]+"' class='slide' id='code"+data[i]["id"]+"'>" +
						"<textarea style='resize:none;' rows='50' cols='70' readonly>"+data[i]["code"]+"</textarea></li>";
			}
			listeCode+="";
		}
		$(".codeCharge").html(listeCode);
		$("#slide").hide();
		$("#slide").first().show();
	});			
}

function login()
{
	getWithAuthorizationHeader("/v1/login", function(data)
	{
	    $("#login_form").hide();
	    afficheUser(data);
	});
}

function like(idUser,idCode){
	
	
	
	$.ajax(
		    {
			type : 'POST',
			contentType : 'application/json',
			url : "v1/vote/"+idUser+"/"+idCode+"/like",
			dataType : "json",
			data : JSON.stringify(
		    {
				"idUser" : idUser,
				"idCode" : idCode
				
			}),
			
			success : function(data, textStatus, jqXHR)
			{
				
			},
			
			error : function(jqXHR, textStatus, errorThrown)
			{
				console.log('postUser error: ' + textStatus);
			}
		});
}










function dislike(idUser,idCode){
	
	
	
	$.ajax(
		    {
			type : 'POST',
			contentType : 'application/json',
			url : "v1/vote/"+idUser+"/"+idCode+"/dislike",
			dataType : "json",
			data : JSON.stringify(
		    {
				"idUser" : idUser,
				"idCode" : idCode
				
			}),
			
			success : function(data, textStatus, jqXHR)
			{
				
			},
			
			error : function(jqXHR, textStatus, errorThrown)
			{
				console.log('postUser error: ' + textStatus);
			}
		});
}









function profile()
{
	getWithAuthorizationHeader("v1/profile", function (data)
	{
		showProfile(data);
	});
}

function getWithAuthorizationHeader(url, callback)
{
    if($("#userlogin").val() != "")
    {
        $.ajax
        ({
            type: "GET",
            url: url,
            dataType: 'json',
            beforeSend : function(req)
            {
                req.setRequestHeader("Authorization", "Basic " + btoa($("#userlogin").val() + ":" + $("#passwdlogin").val()));
            },
            success: callback,
            error : function(jqXHR, textStatus, errorThrown)
            {
                alert('error: ' + textStatus);
            }
        });
    }
    else
    {
        $.getJSON(url, function(data)
        {
            afficheUser(data);
        });
    }
}

function postUser(name, alias, email, pwd)
{
    postUserGeneric(name, alias, email, pwd, 'v1/user/')
}

function postUserGeneric(name, alias, email, pwd, url)
{
	console.log("postUserGeneric " + url)
	$.ajax(
	    {
		type : 'POST',
		contentType : 'application/json',
		url : url,
		dataType : "json",
		data : JSON.stringify(
	    {
			"name" : name,
			"alias" : alias,
			"email" : email,
			"password" : pwd,
			"id" : 0
		}),
		
		success : function(data, textStatus, jqXHR)
		{
			afficheUser(data);
			$().val(data["id"]);
			$(".id").attr("value", "New text");
		},
		
		error : function(jqXHR, textStatus, errorThrown)
		{
			console.log('postUser error: ' + textStatus);
		}
	});
}

function listUsers()
{
    listUsersGeneric("v1/user/");
}

function listUsersGeneric(url)
{
	$.getJSON(url, function(data)
	{
		afficheListUsers(data)
	});
}

function afficheUser(data)
{
	console.log(data);
	
	$('#profileLink').html(data["name"]);
	$('.title').html("<input class='id' type='hidden' value='" + data["id"] + "'/>");
	$(".container").hide();
	$("#navigation").show();
	$("#whenLogIn").show();
	$("#bigLogo").hide();
	chargeCode();
	$("#reponse").html(userStringify(data));
}

function showProfile(data) {
	$("#whenLogIn").hide();
	$("#showProfile").show();
	$("#navigation").show();
	$("#bigLogo").hide();
	$(".title").html("Votre profil <span class='name'>" + data["name"] + "</span><input class='id' type='hidden' value='" + data["id"] + "'/>");
}

function indexRedirect() {
	$("#whenLogIn").show();
	$("#showProfile").hide();
}

function showUploadCode() {
	$("#showProfile").hide();
	$("#upCode").show();
	$(".title").html("Enter code: ");
}

function afficheListUsers(data)
{
	var ul = document.createElement('ul');
	ul.className = "list-group";
	var index = 0;
	
	for(index = 0; index < data.length; ++index)
	{
	    var li = document.createElement('li');
	    
	    li.className = "list-group-item";
		li.innerHTML = userStringify(data[index]);
		ul.appendChild(li);
	}
	
	$("#reponse").html(ul);
}

function userStringify(user)
{
    return user.id + ". " + user.name + " &lt;" + user.email + "&gt;" + " (" + user.alias + ")";
}