document.querySelector("#reset").addEventListener("click", test);//innerHTML = "&nbsp;";

function test()
{
	document.querySelector(".error-container").innerHTML = "&nbsp;";
	
	document.querySelector("#userlogin").style.backgroundColor = "rgba(255, 0, 0, 0)";
}



document.querySelector(".error-container").innerHTML = "&nbsp;";

function checkLogin()
{
	const error = document.querySelector(".error-container");
	const val   = document.querySelector("#userlogin");
	
	const regex = /^[A-Za-z0-9]{1,20}$/;
	
	if(val.value.length == 0)
	{
		error.innerHTML = "Veuillez saisir un identifiant !";
		
		val.style.backgroundColor = "rgba(255, 0, 0, 0.1)";
		
		return false;
	}
	else if(val.value.length < 6)
	{
		if(regex.test(val.value))
		{
			error.innerHTML = "Doit être compris entre 6 et 20 caractères !";
			
			val.style.backgroundColor = "rgba(255, 0, 0, 0.1)";
		}
		else
		{
			error.innerHTML = "Doit être uniquement composé de lettre et/ou de chiffre !";
			
			val.style.backgroundColor = "rgba(255, 0, 0, 0.1)";
		}

		return false;
	}
	else if(val.value.length >= 6 && val.value.length <= 20)
	{
		if(regex.test(val.value))
		{
			return true;
		}
		else
		{
			error.innerHTML= "Doit être uniquement composé de lettre et/ou de chiffre !";
			
			val.style.backgroundColor = "rgba(255, 0, 0, 0.1)";

			return false;
		}
	}
	else
	{
		error.innerHTML= "Doit être compris entre 6 et 20 caractères !";
		
		val.style.backgroundColor = "rgba(255, 0, 0, 0.1)";

		return false;
	}
}